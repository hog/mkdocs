FROM melopt/mkdocs

## Install the mkdocs system
RUN apk --update --upgrade add gcc musl-dev jpeg-dev zlib-dev libffi-dev cairo-dev pango-dev gdk-pixbuf-dev 
RUN pip3 install --upgrade pip \
    && pip3 install mkdocs-awesome-pages-plugin \
    && pip3 install mkdocs-git-revision-date-localized-plugin \
    && pip3 install mkdocs-pdf-export-plugin \
    && rm -rf "$HOME/.cache"

## Define our Entrypoint script
ENTRYPOINT ["/cmds"]

## The default command for the entrypoint script, show the help message
CMD ["help"]