# -*- coding: utf-8 -*-
"""
MkDocs Configuration Library Tests
"""

import tempfile
import unittest

import mkdocs_configuration_lib as lib


class TestMkdocsConfigurationLib(unittest.TestCase):
    """
    Tests for the MkDocs Configuration Library
    """

    def _helper_test_read_configuration(self, lines, yaml_dict):
        with tempfile.NamedTemporaryFile(mode='w') as temporary_file_object:
            temporary_file_object.write('\n'.join(lines))
            temporary_file_object.seek(0)
            self.assertEqual(
                lib.read_configuration(
                    temporary_file_object.name
                ),
                yaml_dict
            )

    def test_read_configuration(self):
        """
        Tests for `read_configuration`.
        """

        test_cases_read_configuration = (

            ([
                '}',
            ], {}),

            ([
                'foo: bar',
            ], {'foo': 'bar'}),

        )

        for lines, yaml_dict in test_cases_read_configuration:
            with self.subTest(lines=lines, yaml_dict=yaml_dict):
                self._helper_test_read_configuration(
                    lines,
                    yaml_dict
                )

    def test_write_configuration(self):
        """
        Tests for `write_configuration`.
        """

        with tempfile.NamedTemporaryFile(mode='w') as temporary_file_object:
            self.assertEqual(
                lib.write_configuration(
                    {'foo': 'bar'},
                    temporary_file_object.name
                ),
                True,
            )

    def _helper_test_get_markdown_header(self, lines, header):
        with tempfile.NamedTemporaryFile(mode='w') as temporary_file_object:
            temporary_file_object.write('\n'.join(lines))
            temporary_file_object.seek(0)
            self.assertEqual(
                lib.get_markdown_header(
                    temporary_file_object.name
                ),
                header
            )

    def test_get_markdown_header(self):
        """
        Tests fot `get_markdown_header`.
        """

        test_cases_get_markdown_header = (

            ([
                '# Header',
            ], 'Header'),

            ([
                'content',
                '    # Header   ',
                'content',
            ], 'Header'),

            ([
                'Header',
            ], None),

            ([
                'Header',
                '======',
            ], 'Header'),

            ([
                'Header',
                '-=====',
                '',
                'content',
            ], None),

            ([
                'Header',
                '===',
            ], 'Header'),

            ([
                'content',
                'Header',
                '======',
                'content',
            ], 'Header'),

            ([
                '# Header',
                '# Another header',
            ], 'Header'),

            ([
                ' # Header ',
                'content',
                '',
                '# Another header',
            ], 'Header'),

            ([
                'Header',
                '======',
                '',
                'content',
                '',
                'Another header',
                '==============',
            ], 'Header'),

            ([
                '# Header',
                '========',
                '',
                'content',
                '',
                'Another header',
                '==============',
            ], 'Header'),

        )

        for lines, header in test_cases_get_markdown_header:
            with self.subTest(lines=lines, header=header):
                self._helper_test_get_markdown_header(
                    lines,
                    header
                )


if __name__ == '__main__':
    unittest.main()
