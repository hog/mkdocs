# -*- coding: utf-8 -*-
"""
MkDocs Configuration Library
"""

import os
import re
import yaml

HOMEPAGE_FILES = ['index.md', 'README.md']
DOCUMENTATION_DIRECTORY = 'docs'
CONFIGURATION_FILE = 'mkdocs.yml'

DIRECTORY_PREFIX_PATTERN = re.compile(
    r'^(\d*)?[\.\-\_\s\)]?(.*)?$',
    re.IGNORECASE
)


def read_configuration(configuration_file=CONFIGURATION_FILE):
    """
    Return the MkDocs configuration as a Python dictionary as read from the
    `configuration_file`.
    """

    # Make sure the configuration file is there before reading it.
    # If not, just return an empty dictionary at the end.
    if os.path.isfile(configuration_file):
        with open(configuration_file, 'r') as configuration_file_object:
            # If we fail to load the configuration file as a valid YAML file
            # return an empty dictionary.
            try:
                return yaml.load(configuration_file_object)
            except Exception:
                return {}
    return {}


def write_configuration(configuration, configuration_file=CONFIGURATION_FILE):
    """
    Store the given MkDocs configuration (Python dictionary) in the
    `configuration_file` with YAML syntax.
    """

    # Write the YAML file with a more "human-readable" style
    # (`default_flow_style=False`), in case someones wants to look at it.
    try:
        with open(configuration_file, 'w') as configuration_file_object:
            configuration_file_object.write(
                yaml.dump(configuration, default_flow_style=False)
            )
        return True
    except Exception:
        return False


def get_markdown_header(file_path):
    """
    Return the main header of the Markdown file found in the `file_path`.

    Look for headers like:
    `# Header`
    or
    ```
    Header
    ======
    ```
    """

    with open(file_path, "r") as file_object:
        # Keep the previous file line as well, in case we are dealing with
        # headers that use the '=' notation in the following line.
        previous_file_line = None
        # Get the first available file line.
        file_line = file_object.readline()
        # Keep going through the file lines as long as they have content
        # (even newlines '\n').
        while file_line:
            # Clean the leading and trailing whitespace off the current line,
            # it should not affect a relatively well-formed Markdown file.
            file_line = file_line.strip()
            # If the current line is only using the '=' notation, then it means
            # the previous line was the main header.
            if '=' in file_line and len(file_line) == file_line.count('='):
                # Make sure there is some actual content there before
                # returning it.
                if previous_file_line:
                    return previous_file_line
            # If the current line starts with '# ' then it means it is the main
            # header.
            if file_line[0:2] == '# ':
                # Separate the content from the notation.
                header = file_line[2:].strip()
                # Make sure there is some actual content there before
                # returning it.
                if header:
                    return header
            # Get the next available file line, and just before doing that,
            # save the current file line as the previous file line for the
            # next iteration.
            previous_file_line = file_line
            file_line = file_object.readline()
    return None


def guess_site_name(documentation_directory=DOCUMENTATION_DIRECTORY):
    """
    Return the guessed `site_name` based on the available homepage files.
    """

    # Iterate through the available homepage files and as long as one is found,
    # stop lookinf for others.
    for homepage_file in HOMEPAGE_FILES:
        homepage_path = os.path.join(
            documentation_directory,
            homepage_file
        )
        if os.path.isfile(homepage_path):
            break
        else:
            homepage_path = None
    # If at least one homepage file was found get the header from it and return
    # it as the `site_name`. Otherwise return `None`.
    if homepage_path is not None:
        return get_markdown_header(homepage_path)
    return None


def generate_nav(
        documentation_directory=DOCUMENTATION_DIRECTORY,
        strip_directory_prefix=False
):
    """
    Return the `nav` (format and layout of the global navigation for the site)
    based on the file and directory structure of the documentation directory.
    """

    def _process_path(path, nav_section):

        # For the current path get the path itself (`dirpath`), as well as the
        # list of directories (`dirnames`) and files (`filenames`) in it.
        dirpath, dirnames, filenames = next(os.walk(path))

        # First start with the files in the current path, go through them
        # alphabetically and add them as items to the current `nav` section.
        # Each item's title is taken from the Markdown header of that file.
        # NOTE: give 'alphabetical' priority to the "homepage" files.
        for filename in sorted(
            filenames,
            key=lambda i: i in HOMEPAGE_FILES and chr(0) or str.lower(i)
        ):
            # If the file is one of the "homepage" files then give the special
            # "Home" title to it instead of its Markdown header. Make sure to
            # ignore non-Markdown files.
            if os.path.splitext(filename)[1] != '.md':
                continue
            elif filename in HOMEPAGE_FILES:
                item_title = 'Home'
            else:
                item_title = get_markdown_header(os.path.join(
                    dirpath, filename
                ))
            # Since we start processing from the root documentation directory,
            # all the paths contain it. However, MkDocs expects all paths in
            # the `nav` to be relative to whithin the root documentation
            # directory. Therefore we remove the root documentation directory
            # from all paths before adding them to their `nav` section.
            item_path = os.path.join(
                dirpath, filename
            ).lstrip(documentation_directory).lstrip('/')
            # Append the item to the current `nav` section.
            nav_section.append({item_title: item_path})

        # Once the files have been processed, start processing the directories
        # in alphabetical order too.
        for dirname in sorted(dirnames):
            # Instantiate a placeholder list for this directory's `nav` section
            # and append it to the end of the current `nav` section. The title
            # is the directory's name, with the option to strip the numbering
            # prefix at the beginning (`strip_directory_prefix`).
            dirname_nav_section = []
            nav_section.append({re.search(
                DIRECTORY_PREFIX_PATTERN, dirname
            ).group(
                strip_directory_prefix and 2 or 0
            ): dirname_nav_section})
            # Process this directory's path and store the resulting format and
            # layout to this directory's placeholder `nav` section list.
            _process_path(os.path.join(dirpath, dirname), dirname_nav_section)

    # Instantiate a placeholder list for the final `nav`
    nav = []

    # Start by processing the root documentation directory. This will then
    # recursively also process all the children directories. The resulting
    # format and layout will be stored in the placeholder list `nav`.
    _process_path(documentation_directory, nav)

    return nav
