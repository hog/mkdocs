#!/usr/local/bin/python
# -*- coding: utf-8 -*-
"""
MkDocs Configuration CLI
"""

import click

import mkdocs_configuration_lib as lib


@click.command()
@click.option(
    '--site-name',
    'site_name',
    default=None,
    help='The title of the documentation.'
)
@click.option(
    '--site-description',
    'site_description',
    default=None,
    help='The description of the documentation.'
)
@click.option(
    '--site-author',
    'site_author',
    default=None,
    help='The author of the documentation.'
)
@click.option(
    '--site-url',
    'site_url',
    default=None,
    help='The canonical URL of the documentation.'
)
@click.option(
    '--repo-name',
    'repo_name',
    default=None,
    help='The name of the documentation repository.'
)
@click.option(
    '--repo-url',
    'repo_url',
    default=None,
    help='The canonical URL of the documentation repository.'
)
@click.option(
    '--theme-name',
    'theme_name',
    default=None,
    help=('The name of a known installed theme. '
          'Available options: "mkdocs", "readthedocs" and "material". '
          'NOTE: It is not possible to set theme-specific configuration.')
)
@click.option(
    '--plugin',
    'plugins',
    multiple=True,
    help=('The plugin to use when building the site. '
          'Use multiple times to define multiple plugins. '
          'NOTE: It is not possible to set optional configuration settings.')
)
@click.option(
    '--generate-nav',
    'generate_nav',
    is_flag=True,
    help=('Generate the format and layout '
          'of the global navigation for the site.')
)
@click.option(
    '--strip-directory-prefix',
    'strip_directory_prefix',
    is_flag=True,
    help=('When generating the format and layout '
          'of the global navigation for the site, '
          'strip the directory prefix (e.g. "01."). '
          'NOTE: Only applicable when used with "--generate-nav".')
)
def main(
    site_name,
    site_description,
    site_author,
    site_url,
    repo_name,
    repo_url,
    theme_name,
    plugins,
    generate_nav,
    strip_directory_prefix
):
    """Generate the MkDocs configuration."""

    configuration = lib.read_configuration()

    if site_name is not None:
        configuration.update({'site_name': site_name})
    elif 'site_name' not in configuration:
        site_name = lib.guess_site_name()
        if site_name is not None:
            configuration.update({'site_name': site_name})
        else:
            configuration.update({'site_name': 'Documentation'})

    if site_description is not None:
        configuration.update({'site_description': site_description})

    if site_author is not None:
        configuration.update({'site_author': site_author})

    if site_url is not None:
        configuration.update({'site_url': site_url})

    if repo_name is not None:
        configuration.update({'repo_name': repo_name})

    if repo_url is not None:
        configuration.update({'repo_url': repo_url})

    if theme_name is not None:
        configuration.update({'theme': {'name': theme_name}})
    elif 'theme' not in configuration:
        configuration.update({'theme': {'name': 'material'}})

    if plugins:
        configuration.update({'plugins': list(plugins)})
    elif 'plugins' not in configuration:
        configuration.update({'plugins': ['search']})

    if generate_nav and 'nav' not in configuration:
        nav = lib.generate_nav(strip_directory_prefix=strip_directory_prefix)
        if nav:
            configuration.update({'nav': nav})

    lib.write_configuration(configuration)


if __name__ == '__main__':
    main()
