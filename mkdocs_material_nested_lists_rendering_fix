#!/usr/local/bin/python
# -*- coding: utf-8 -*-
"""
MkDocs Material theme fix for nested lists rendering.
<https://github.com/squidfunk/mkdocs-material/issues/508>
"""

import click
import os
import sys

import mkdocs_configuration_lib as lib


@click.command()
def main(
):
    """MkDocs Material theme fix for nested lists rendering."""

    # Read the existing configuration from `mkdocs.yml`.
    configuration = lib.read_configuration()

    # If no configuration was found do nothing and exit successfully
    # and silently.
    if not configuration:
        sys.exit(0)

    # Make sure the configuration includes the `theme`, otherise exit
    # successfully and silently.
    theme = configuration.get('theme')
    if theme is None:
        sys.exit(0)

    # Make sure the theme configuration includes the `name` and the name is
    # `material`, otherise exit successfully and silently.
    theme_name = theme.get('name')
    if theme_name is None or theme_name != 'material':
        sys.exit(0)

    # Create the `stylesheets` directory in the documentation directory if it
    # doesn't already exist.
    try:
        os.mkdir(os.path.join(lib.DOCUMENTATION_DIRECTORY, 'stylesheets'))
    except FileExistsError:
        # Since the `stylesheets` directory already existed, check if a
        # `material_nested_lists_rendering.css` file also existed already.
        # If yes, exit successfully and silently.
        if os.path.isfile(os.path.join(
            lib.DOCUMENTATION_DIRECTORY,
            'stylesheets',
            'material_nested_lists_rendering.css'
        )):
            sys.exit(0)

    # Create the appropriate stylesheet file.
    with open(os.path.join(
        lib.DOCUMENTATION_DIRECTORY,
        'stylesheets',
        'material_nested_lists_rendering.css'
    ), 'w') as file_object:
        file_object.write('\n'.join([
            'article ul ul {',
            '  list-style-type:  circle !important;',
            '}',
            '',
            'article ul ul ul {',
            '  list-style-type:  square !important;',
            '}',
        ]))

    # Get the `extra_css` from the configuration. If it's not there get it as
    # an empty list.
    extra_css = configuration.get('extra_css', [])
    # Append the recently created file `material_nested_lists_rendering.css`
    # to the `extra_css` list.
    extra_css.append(os.path.join(
        'stylesheets',
        'material_nested_lists_rendering.css'
    ))
    # Update the configuration with the updated `extra_css` list.
    configuration.update({'extra_css': extra_css})

    # Write the updated configuration to `mkdocs.yml`.
    lib.write_configuration(configuration)


if __name__ == '__main__':
    main()
